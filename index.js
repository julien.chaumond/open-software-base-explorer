'use strict';

const express = require('express');
const i18n = require("i18n");
const lastModified = require('cache-cache');
const md = require('marked');
const morgan = require('morgan');
const path = require('path');
const validator = require('validator');

const app = express();

// Config
const port = require('./config').port;
const locales = require('./config').locales;
i18n.configure({
  locales: locales,
  directory: path.join(__dirname, '/locales'),
  indent: '  '
});

app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'jade');
app.enable('trust proxy');

// Global middlewares
if (app.get('env') === 'development') {
  app.use(morgan('dev'));
}

app.use((req, res, next) => {
  res.locals.baseLocation = req.protocol + '://' + req.hostname +
    (app.get('env') === 'production' ? '' : ':' + port);
  res.locals.md = md;
  res.locals.validator = validator;

  return next();
});

app.use('/static', express.static('static', {maxAge: '365d'}));

// If we are on /, redirect to supported locale
app.get('/', i18n.init, (req, res) => {
  return res.redirect('/' + req.locale + '/');
});

app.use(lastModified());

// API routes
app.use('/api', require('./routes'), (req, res, next) => {
  var err = new Error('Not found.');
  err.status = 404;

  return next(err);
}, (err, req, res, next) => { // eslint-disable-line no-unused-vars
  if (err.status === 500 || typeof err.status === 'undefined') {
    return res.status(500).send({error: 'Server error.', status: 500});
  }
  return res.status(err.status).send({error: err.message, status: err.status});
});

// HTML pages routes, never reached if API is matched
app.use(i18n.init);
app.param('locale', (req, res, next, locale) => {
  if (locales.indexOf(locale) === -1) {
    var err = new Error('Language not found.');
    err.status = 404;

    return next(err);
  }

  i18n.setLocale(req, locale);

  res.locals.alternateLocales = locales.map(alternateLocale => ({
    name: alternateLocale,
    url: req.url.replace(locale, alternateLocale)
  }));

  return next();
});

app.use('/:locale', require('./routes'));

app.listen(3000, '127.0.0.1');
